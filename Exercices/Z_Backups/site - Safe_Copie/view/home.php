<?php
/*
Autor   : Luca.BASSI
Date    : 16.12.2019
*/

ob_start();
?>


            <!--Edit Main Content Area here-->
            <div class="span12" id="divMain">
                <h1>Nos activités</h1>

                <p><strong>Rent A Snow</strong> est spécialisée dans la location de snows. Nous avons tout types de
                    modèles :
                <ul>
                    <li>des plus récents au plus anciens,
                    <li>pour débutants ou confirmés,
                    <li>pour de la piste ou du hors-piste
                </ul>
                La location peut se faire au jour, à la semaine, au mois ou à la saison.
                </p>
                <p>
                    Nous proposons aussi des cours privés ou en petits groupe (4 personnes maximum) pour tous les
                    niveaux avec des moniteurs certifiés par l'école suisse de snowboard au prix de 60.- /heure.
                </p>

                <br/>
                <br/>




<!--
                <header>
                    <h1>Simple Confirmation Popup</h1>
                </header>
-->

           <div class="contentArea">

                    <div class="divPanel notop page-content">

                        <div class="breadcrumbs">
                            <a href="../gabarit.php">Home</a> &nbsp;/&nbsp; <span>Services</span>
                        </div>

                        <div class="row-fluid">
                            <!--Edit Main Content Area here-->
                            <div class="span12" id="divMain">

                                <br />
                                <br />


                                <div class="row-fluid">

                                    <div class="span3">
                                        <div class="box">
                                            <i class="general foundicon-settings"></i>
                                            <h3 class="title">Web Design</h3> <hr/>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce cursus, sapien nec dignissim porta.
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce cursus, sapien nec dignissim porta.
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                            <p><a class="btn btn-info" style="margin:5px 0px 15px;">Learn more</a></p>
                                        </div>
                                    </div>

                                    <div class="span3">
                                        <div class="box">
                                            <i class="general foundicon-website"></i>
                                            <h3 class="title">Web Design</h3> <hr/>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce cursus, sapien nec dignissim porta.
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce cursus, sapien nec dignissim porta.
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                            <p><a class="btn btn-info" style="margin:5px 0px 15px;">Learn more</a></p>
                                        </div>
                                    </div>

                                    <div class="span3">
                                        <div class="box">
                                            <i class="general foundicon-edit"></i>
                                            <h3 class="title">Custom Themes</h3> <hr/>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce cursus, sapien nec dignissim porta.
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce cursus, sapien nec dignissim porta.
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                            <p><a class="btn btn-info" style="margin:5px 0px 15px;">Learn more</a></p>
                                        </div>
                                    </div>

                                    <div class="span3">
                                        <div class="box">
                                            <i class="general foundicon-tools"></i>
                                            <h3 class="title">SEO</h3> <hr/>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce cursus, sapien nec dignissim porta.
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce cursus, sapien nec dignissim porta.
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                            <p><a class="btn btn-info" style="margin:5px 0px 15px;">Learn more</a></p>
                                        </div>
                                    </div>

                                </div>

                                <hr>
                                <br>


                                            <!--End Accordians here-->
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!--End Main Content-->

<?php
$contenu=ob_get_clean();
require 'gabarit.php';
?>