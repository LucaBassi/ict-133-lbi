<?php

/*
Autor   : Luca.BASSI
Date    : 06.01.2020
*/


/**
 * @return mixed
 */
function extractData()
{
    $dataDirectory = "data";
    $dataFileName = "mydata.json";
    if (file_exists("$dataDirectory/$dataFileName")) // the file already exists -> load it
    {
        $data = json_decode(file_get_contents("$dataDirectory/$dataFileName"), true);
    }
    return $data;
}


function extractSnowsData()
{
    $dataDirectory = "data";
    $dataFileName = "dataSnows.json";
    if (file_exists("$dataDirectory/$dataFileName")) // the file already exists -> load it
    {
        $dataSnow = json_decode(file_get_contents("$dataDirectory/$dataFileName"), true);
    }
    return $dataSnow;
}

function testExtractSnowsData()
{
    $dataDirectory = "data";
    $dataFileName = "dataSnows.json";
    if (file_exists("$dataDirectory/$dataFileName")) // the file already exists -> load it
    {
        $dataSnow = json_decode(file_get_contents("$dataDirectory/$dataFileName"), true);
    }
    return $dataSnow;
}


function check()
{
    $data = extractData();
    foreach ($data['person'] as $datas) {
        if ($datas['username'] == $_POST['username'] && $datas['password'] == $_POST['password']) {

            $connected = true;
            return $connected;
            return true;

        } else {

            $connected = false;
        }

    }
    return $connected;
}


function checkType()
{

    $_SESSION['userType'] = null;
    $data = extractData();
    foreach ($data['person'] as $datas) {
        // $datas["username"]  $_POST["username"]
        if ($datas["username"] == $_SESSION["username"]) {

            $userType = $datas["userType"];
            $_SESSION{'userType'} = $userType;
            return $userType;
            //  return true;

        }

        /*elseif ($datas['userType'] == 0 && $datas['username'] == $_POST['username']) {

            $userType = 'user';
            $_SESSION['userType'] = $userType;
            return $userType;
        } else {
            $userType = 'user';
            $_SESSION['userType'] = $userType;
            return $userType;
        }*/

    }
    // return $userType;
}


function checkLogin()
{
    /*    $_POST['checkLogin'] = '';
        $_POST['goCheckLogin'] = '';*/

    $connected = check();
    if (isset($connected)) {
        if ($connected == true) {

            $_SESSION['username'] = $_POST['username'];
            checkType();
            home();
            //   require 'view/home.php';


        }
    } else {

        require 'view/login.php';
    }

}


function creatAccount()
{

    unset($_POST['error']);
    $_POST['creatAccount'] = '';
    $newPerson = extractData();
    if (isset($_POST['newUsername'])) {
        if ($_POST['newPassword'] != $_POST['newPassword2']) {
            $_POST['error'] = 'Vos mot de passe ne correspndent pas ! :(';
            require 'view/creatUser.php';
        }

        if (isLoggedIn() == true) {
            if (isAdmin() == true) {
                //  echo 'yes';
                if ($_POST["userType"] == 'admin') {
                    $newData['userType'] = 1;
                } elseif ($_POST["userType"] == 'user') {

                    $newData['userType'] = 0;
                    $_SESSION['adminUser']['success'] = 'Success';
                    $_SESSION['adminUser']['msg'] = 'Vous avez ajouté un User avec les droits : ';
                    $_SESSION['adminUser']['user'] = $_POST["userType"];
                } else {
                    $newData['userType'] = 0;
                    $_SESSION['adminUser']['success'] = 'Success';
                    $_SESSION['adminUser']['msg'] = 'Vous avez ajouté un User avec les droits : ';
                    $_SESSION['adminUser']['user'] = $_POST["userType"];

                }

                // $newData['userType'] = 1;
                goCreateUserAdmin();
            } else {
                $newData['userType'] = 0;
                $_SESSION['adminUser']['success'] = 'Success';

                $_SESSION['adminUser']['msg'] = 'Vous avez ajouté un User avec les droits : ';
                $_SESSION['adminUser']['user'] = $_POST["userType"];
            }
        } else {
            $newData['userType'] = 0;
        }
        $newData['username'] = $_POST['newUsername'];
        $newData['password'] = $_POST['newPassword'];

        $newPerson{"person"}[] = $newData;

        $dataDirectory = "data";
        $dataFileName = "mydata.json";

        file_put_contents("$dataDirectory/$dataFileName", json_encode($newPerson, true));

        require 'view/login.php';
    }
}


function selectSnow()
{
    $dataSnows = extractSnowsData();
//echo $prix;
    echo $_POST['id'];
    $id = $_POST['id'];

    foreach ($dataSnows[0]['articles']['snows'] as $snowsTo) {
        $stockSnows = $snowsTo;

        foreach ($stockSnows as $key => $val) {
            if ($key == "photo" && $stockSnows['id'] == $id) {
                // echo "<img style='alignment=center;max-width: 300px  ' src=" . $val . "/><br>";
            }
            if ($stockSnows['id'] == $id) {
                if ($stockSnows['id'] == $id) {
                    if ($key == "marque") {
                        $marqueKey = $key;

                        $marque = $val;

                    }
                    if ($key == "modele") {
                        $modeleKey = $key;
                        $modele = $val;
                    }
                    if ($key == "taille") {
                        $tailleKey = $key;
                        $taille = $val;
                    };
                    if ($key == "Quantité") {
                        $quantiteKey = $key;
                        $quantite = $val;
                    };


                    if ($key == "dispo") {
                        $dispoKey = $key;
                        if ($val == 1)
                            $dispo = "Oui !";
                    }

                    if ($key == "prix") {
                        $prixKey = $key;
                        $prix = $val;
                        $prix['prix'] = $prix;
                    }

                    return $prix;
                }
            }
        }
    }
    return $dataSnows;
}

/*
if ($_POST['creatAccount'] = 'creatAccount') {
    $_POST['creatAccount'] = '';
    creatAccount();
}


if ($_POST['goCheckLogin'] = 'goCheckLogin') {
    $_POST['checkLogin'] = '';
    checkLogin();
}
*/

function products()
{
    $dataSnows = extractSnowsData();

    //   require 'view/magasin.php';
    return $dataSnows;
}

function addSnows()
{

    $snows = extractSnowsData();

    $newSnow['marque'] = $_POST['marque'];
    $newSnow['modele'] = $_POST['modele'];

    $taille = $_POST['taille'];
    $newSnow['taille'] = (int)$taille;

    $newSno['prix'] = (int)$_POST['prix'];
    $newSnow['quantité'] = (int)$_POST['quantite'];

    $newSnow['dispo'] = $_POST['dispo'];


    $json_decode_str = $snows;
    $my_index = 1;
    foreach ($json_decode_str[0]["articles"]['snows'] as $snow) {
        $my_index++;
    }
    //  $snowPosition = 'snow' . $my_index;
    $newSnow['id'] = $my_index;
    $my_index = 0;

    $dir = "upload/";
    $image = $_FILES['photo']['name'];
    $temp_name = $_FILES['photo']['tmp_name'];

    if ($image != "") {
        if (file_exists($dir . $image)) {
            $image = time() . '_' . $image;
        }
        //rename image like : "index-firstname-lastname-imagename.extension" and add is relative id (from my_index out of foreach)
        //$extensionFile=$image['extension'];
        $image = $my_index . '-' . $newSnow['marque'] . '-' . $newSnow['modele'] . '-' . $image;
        $fdir = $dir . $image;
        move_uploaded_file($temp_name, $fdir);

        $newSnow['photo'] = $fdir;
    }

    $snows[0]['articles']['snows'][] = array_values($newSnow);

    $dataDirectory = "data";
    $dataFileName = "dataSnows.json";
    file_put_contents("$dataDirectory/$dataFileName", json_encode($snows));
}


function delASnows()
{

    $ExtractSows = extractSnowsData();


    $json_decode_str = $ExtractSows;
    $my_index = 0;
    $yes = 1;
    foreach ($json_decode_str[0]["articles"]['snows'] as $snow) {

        //   $snowPos='snow'.$my_index;

        if ($json_decode_str[0]["articles"]["snows"][$my_index]["id"] == $_POST['id']) {
            unset($json_decode_str[0]["articles"]["snows"][$my_index]);

            $yes++;
        }
        $my_index++;
    }
 //   $snows[0]['articles']['snows'][] = $newSnow;

    $newArray2= array_values($json_decode_str[0]["articles"]["snows"]);
    $newArray[0]["articles"]['snows'][] =$newArray2;
    //   $newArray2[0]["articles"]['snows'][]=$newArray;
    $dataDirectory = "data";
    $dataFileName = "dataSnows.json";
    file_put_contents("$dataDirectory/$dataFileName", json_encode($newArray));
}




