<?php

session_start();


function home()
{
    $_GET['action'] = "home";
    //include "";
    require 'view/home.php';
   // echo 'wesh';
}

function login()
{
    $_GET['action'] = "login";
    require 'view/login.php';
}

function logout()
{
    unset($_SESSION['username']);
    //session_destroy();

    $_GET['action'] = "home";
    //unset($_SESSION);
    require 'view/home.php';
}

function isLoggedIn()
{
    if (isset($_SESSION["username"])) {
        return true;
    }else{
        return false;
    }
}

function goCheckLogin()
{
    unset($_POST['goCheckLogin']);
    $_GET['action'] = "checkLogin";
    require_once 'model/model.php';

    checkLogin();
    isAdmin();
}

function creatUser()
{
    $_GET['action'] = "creatUser";
    require 'view/creatUser.php';

}

function goCreatAccount()
{
    $_GET['action'] = "creatAccount";
    require 'model/model.php';
    creatAccount();
}

function goMagasin()
{
    $_GET['action'] = "goMagasin";
    require 'model/model.php';
    $dataSnows = products();
    require 'view/magasin.php';
}

function addASnow()
{

    require 'view/newSnow.php';
}

function goCommand()
{
    $_GET['action'] = "goCommand";
    require 'model/model.php';
    $dataSnows = products();
    //$dataSnows= selectSnow();
    require 'view/snowDetails.php';
}

function goPanier()
{
    $_GET['action'] = "goPanier";
    require 'view/panier.php';
}

function goCreateUserAdmin()
{
    //   $_GET['action'] = "createUserAdmin";

    require 'view/createUserAdmin.php';
}

function createUserAdmin()
{
 //   $_GET['action'] = "createUserAdmin";

    require 'view/createUserAdmin.php';
}

function checkIfsnowExist()
{
    $checkSnow = true;

    if (isset($_SESSION['snow'])) {
        $myIndex = 0;

        foreach ($_SESSION['snow'] as $wesh) {

            if ($_SESSION['snow'][$myIndex]{'id'} == $_POST['id']) {

                $checkSnow = false;
            } else {
                $checkSnow = true;
            }

            $myIndex++;
        }
    }

    return $checkSnow;

}

/**
 *
 */
function addPanier()
{
    unset($_SESSION['success']);
    $check = checkIfsnowExist();
    if ($check == true) {


//    unset($_SESSION['snow']);
        unset($_POST['error']);
        if ($_POST['qtySelect'] != 0 && $_POST['qtySelect'] > 0) {

            if ($_POST['qtySelect'] >= $_POST['totalQty']) {
                $_POST['error'] = 'Quantité selectionnée trop grande !';
                require 'model/model.php';
                $dataSnows = products();
                require 'view/snowDetails.php';
            } elseif ($_POST['qtySelect'] < $_POST['totalQty']) {
                $_GET['action'] = "addPanier";
                $myIndex = 1;
                //  $test = count($_SESSION['snow']);

                $myIndex = 0;
                if (isset($_SESSION['snow'])) {
                    foreach ($_SESSION['snow'] as $wesh) {
                        $myIndex++;
                    }
                }


                $_SESSION['snow'][$myIndex]{'qtySel'} = $_POST['qtySelect'];
                $_SESSION['snow'][$myIndex]{'totalQty'} = $_POST['totalQty'];
                $_SESSION['snow'][$myIndex]{'id'} = $_POST['id'];

                $_SESSION['snow'][$myIndex]{'marque'} = $_POST['marque'];
                $_SESSION['snow'][$myIndex]{'modele'} = $_POST['modele'];
                $_SESSION['snow'][$myIndex]{'taille'} = $_POST['taille'];
                $_SESSION['snow'][$myIndex]{'photo'} = $_POST['photo'];


                $marque = $_SESSION['snow'][$myIndex]{'marque'};
                $modele = $_SESSION['snow'][$myIndex]{'modele'};
                $taille = $_SESSION['snow'][$myIndex]{'taille'};
                $selectQty = $_SESSION['snow'][$myIndex]{'marque'};
                $photo = $_SESSION['snow'][$myIndex]{'photo'};
                $myIndex = 0;
                /*
                            foreach ($_SESSION['snow'] as $wesh) {
                                $myIndex++;
                                $myIndex++;
                }*/


                require 'model/model.php';
                $dataSnows = products();
                $_SESSION['success']['marque'] = $marque;
                $_SESSION['success']['modele'] = $modele;
                $_SESSION['success']['taille'] = $taille;
                $_SESSION['success']['qtySel'] = $selectQty;
                $_SESSION['success']['success'] = 'Success';

                /*
                                echo "<h3>Success!</h3>";

                                echo "<h5>$marque</h5>";
                                echo "<h5>$modele</h5>";
                                echo "<h5>$modele</h5>";
                                echo "<h5>$modele</h5>";
                                             */

                //   echo "<h5><img src=.$photo.</h5>";

                //      echo "<h3>$wesh</h3>";

                //        echo $_SESSION['snow']['qty'];


                require 'view/magasin.php';
            } else {
                $_POST['error'] = 'Erreur de Quantité ! !';
                require 'model/model.php';
                $dataSnows = products();
                require 'view/snowDetails.php';
            }
        } else {
            $_POST['error'] = 'Erreur de Quantité ! !';
            require 'model/model.php';
            $dataSnows = products();
            require 'view/snowDetails.php';
        }
    }
    if ($check == false) {
        $_POST['error'] = 'Vous avez deja commander cet article !';
        require 'model/model.php';
        $dataSnows = products();
        require 'view/magasin.php';

    }
}

function delPanier()
{
    // session_destroy();

    $_GET['action'] = "home";
    unset($_SESSION['snow']);
    require 'view/home.php';
}

function userDelSnow()
{
    $myIndex = 0;
    if (count($_SESSION['snow']) > 0) {
        if (isset($_SESSION['snow'])) {

            if (isset($_SESSION['snow'][$myIndex])) {
                foreach ($_SESSION['snow'][$myIndex] as $wesh) {
                    if (isset($_SESSION['snow'][$myIndex]{'id'})) {
                        if ($_SESSION['snow'][$myIndex]{'id'} == $_POST['id']) {
                            //  $_SESSION['snow'][$myIndex]="";
                            unset($_SESSION['snow'][$myIndex]);
                            $rebuild = array_values($_SESSION['snow']);
                            $_SESSION['snow'] = $rebuild;
                        }
                    }
                    //    array_unshift($_SESSION['snow'][$myIndex]{'id'},null);
                    /*           $rebuild=sizeof($_SESSION['snow']);
                               for ($i=0;$i<$rebuild;$i++){
                                   $_SESSION['snow'][$i]=$rebuild;
                               }*/


                    //   require 'view/home.php';
                    /*
                                    $_SESSION['snow'][$myIndex]{'qty'} = "";
                                    $_SESSION['snow'][$myIndex]{'id'} = "";
                                    $_SESSION['snow'][$myIndex]{'marque'} ="";
                                    $_SESSION['snow'][$myIndex]{'modele'} ="";
                                    $_SESSION['snow'][$myIndex]{'taille'} = "";
                                    $_SESSION['snow'][$myIndex]{'photo'} = "";

                    */
                    /*            if (count($_SESSION['snow']) == 0) {
                                    unset($_SESSION['snow']);
                                     $myIndex=0;
                                }*/
                    $myIndex++;
                }

                $myIndex = 0;
            }


        }

    }

    goPanier();
    //  require 'view/home.php';
}

function isAdmin()
{
    // require 'model/model.php';

    if (checkType() == true) {
        return true;
    } else {
        return false;
    }
}

function gestion()
{
    $_GET['action'] = "goMagasin";
    require 'model/model.php';
    $dataSnows = products();
    require 'view/gestion.php';
}

function newSnow(){
   require 'model/model.php';
    addSnows();
}

function delSnow(){
   require 'model/model.php';
    delASnows();
}
